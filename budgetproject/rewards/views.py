from django.shortcuts import render, get_object_or_404, redirect
from .models import Reward, Task, Person
from .forms import RewardForm, PersonForm, RadioForm
from django.views.generic import CreateView
from django.utils.text import slugify
from django.http import HttpResponseRedirect, HttpResponse
import json
from django.contrib.auth.models import User
from django.utils import timezone

from random import randint
from django.views.generic import TemplateView
from chartjs.views.lines import BaseLineChartView


# Create your views here.
def reward_list(request):
    reward_list = Reward.objects.all()
    print(reward_list)
    return render(request, 'rewards/reward-list.html', {'reward_list': reward_list})


def reward_detail(request, pk):
    reward = get_object_or_404(Reward, pk=pk)
    return render(request, 'rewards/reward-detail.html', {'reward': reward})

def reward_goods(request):
    reward_list = Reward.objects.all()
    return render(request, 'rewards/reward-goods.html', {'reward_list': reward_list})

def reward_investments(request):
    reward_list = Reward.objects.all()
    return render(request, 'rewards/reward-investments.html', {'reward_list': reward_list})

def reward_blank(request):
    reward_list = Reward.objects.all()
    return render(request, 'rewards/reward-blank.html', {'reward_list': reward_list})

def reward_custom(request):
    reward_list = Reward.objects.all()
    return render(request, 'rewards/reward-custom.html', {'reward_list': reward_list})

def reward_screentime(request):
    reward_list = Reward.objects.all()
    return render(request, 'rewards/reward-screentime.html', {'reward_list': reward_list})

def task_tasklist(request):
    task_list = Task.objects.all()
    return render(request, 'rewards/task-tasklist.html', {'task_list': task_list})

def person_personlist(request):
    person_list = Person.objects.all()
    user_list = User.objects.all()
    user_balance_list = []

    for i in range(len(user_list)):
        user_balance_list.append(str(user_list[i]) + ":" + str(Person.objects.filter(user=User.objects.get(pk=i+1)).latest('action_date').current_balance))

    print(user_balance_list)

    return render(request, 'rewards/person-personlist.html', {'person_list': person_list, 'user_balance_list': user_balance_list})


"""def reward_detail(request, reward_slug):
def reward_detail(request):

    reward = get_object_or_404(Reward, slug=reward_slug)

    if request.method == 'GET':
        category_list = Category.objects.filter(project=project)
        return render(request, 'budget/project-detail.html', {'project': project, 'expense_list': project.expenses.all(), 'category_list': category_list})

    elif request.method == 'POST':
        # process the form
        form = ExpenseForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            amount = form.cleaned_data['amount']
            category_name = form.cleaned_data['category']

            category = get_object_or_404(Category, project=project, name=category_name)

            Expense.objects.create(
                project=project,
                title=title,
                amount=amount,
                category=category,

            ).save()

    elif request.method == 'DELETE':
        id = json.loads(request.body)['id']
        expense = get_object_or_404(Expense, id=id)
        expense.delete()

        return HttpResponse('')

    return HttpResponseRedirect(reward_slug)
    return HttpResponse("hello!")
    """

class RewardCreateView(CreateView):
    model = Reward
    template_name='rewards/add-reward.html'
    fields = ('name', 'reward')

    """
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()

        categories = self.request.POST['categoriesString'].split(',')
        for category in categories:
            Category.objects.create(
            project=Project.objects.get(id=self.object.id),
            name=category
            ).save()

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return slugify(self.request.POST['name'])
    """
def reward_goods(request):
    if request.method == "POST":
        form = RadioForm(request.POST)
        print(form)
        reward_list = Reward.objects.all()
        print("I am in IF!")
        if form.is_valid():
            print("I am in IF-IF!")
            print("Choice field is: ")
            print(form.cleaned_data['choice_field'])
            print("current user balance is original:")
            print(str(Person.objects.filter(user=User.objects.get(pk=1)).latest('action_date').current_balance))
            print("current user balance is request.user:")
            print(str(Person.objects.filter(user=request.user).latest('action_date').current_balance))
            print("task reward is request.user:")
            print("reward amount is : " + str(Reward.objects.get(id=form.cleaned_data['choice_field']).kikas))
            reward_kikas = Reward.objects.get(id=form.cleaned_data['choice_field']).kikas
            person_current_balance = Person.objects.filter(user=request.user).latest('action_date').current_balance
            final_balance = person_current_balance -  reward_kikas
            print("final_balance is : " + str(final_balance))
            Person.objects.create(user=request.user, action_date=timezone.now(), current_balance=final_balance)
            #reward = form.save(commit=False)
            #reward.author = request.user

            #reward.save()
            return redirect('reward_goods')
    else:
        form = RadioForm()
        print("I am in ELSE!")
        reward_list = Reward.objects.all()
        #print(form)
        #print(reward_list)
        #animal = ['cat','dog','mause']
        #how_many = ['one','two','three']
        #data = zip(animal,how_many)
        #data = zip(form, reward_list)
        #print(data)
    return render(request, 'rewards/reward-goods.html', {'form': form, 'reward_list': reward_list})
def rewards_screentime(request):
    if request.method == "POST":
        form = RadioForm(request.POST)
        print(form)
        reward_list = Reward.objects.all()
        print("I am in IF!")
        if form.is_valid():
            print("I am in IF-IF!")
            print("Choice field is: ")
            print(form.cleaned_data['choice_field'])
            print("current user balance is original:")
            print(str(Person.objects.filter(user=User.objects.get(pk=1)).latest('action_date').current_balance))
            print("current user balance is request.user:")
            print(str(Person.objects.filter(user=request.user).latest('action_date').current_balance))
            print("task reward is request.user:")
            print("reward amount is : " + str(Reward.objects.get(id=form.cleaned_data['choice_field']).kikas))
            reward_kikas = Reward.objects.get(id=form.cleaned_data['choice_field']).kikas
            person_current_balance = Person.objects.filter(user=request.user).latest('action_date').current_balance
            final_balance = person_current_balance -  reward_kikas
            print("final_balance is : " + str(final_balance))
            Person.objects.create(user=request.user, action_date=timezone.now(), current_balance=final_balance)
            #reward = form.save(commit=False)
            #reward.author = request.user

            #reward.save()
            return redirect('rewards_screentime')
    else:
        form = RadioForm()
        print("I am in ELSE!")
        reward_list = Reward.objects.all()
        #print(form)
        #print(reward_list)
        #animal = ['cat','dog','mause']
        #how_many = ['one','two','three']
        #data = zip(animal,how_many)
        #data = zip(form, reward_list)
        #print(data)
    return render(request, 'rewards/reward-screentime.html', {'form': form, 'reward_list': reward_list})
def reward_custom(request):
    if request.method == "POST":
        form = RadioForm(request.POST)
        print(form)
        reward_list = Reward.objects.all()
        print("I am in IF!")
        if form.is_valid():
            print("I am in IF-IF!")
            print("Choice field is: ")
            print(form.cleaned_data['choice_field'])
            print("current user balance is original:")
            print(str(Person.objects.filter(user=User.objects.get(pk=1)).latest('action_date').current_balance))
            print("current user balance is request.user:")
            print(str(Person.objects.filter(user=request.user).latest('action_date').current_balance))
            print("task reward is request.user:")
            print("reward amount is : " + str(Reward.objects.get(id=form.cleaned_data['choice_field']).kikas))
            reward_kikas = Reward.objects.get(id=form.cleaned_data['choice_field']).kikas
            person_current_balance = Person.objects.filter(user=request.user).latest('action_date').current_balance
            final_balance = person_current_balance -  reward_kikas
            print("final_balance is : " + str(final_balance))
            Person.objects.create(user=request.user, action_date=timezone.now(), current_balance=final_balance)
            #reward = form.save(commit=False)
            #reward.author = request.user

            #reward.save()
            return redirect('reward_custom')
    else:
        form = RadioForm()
        print("I am in ELSE!")
        reward_list = Reward.objects.all()
        #print(form)
        #print(reward_list)
        #animal = ['cat','dog','mause']
        #how_many = ['one','two','three']
        #data = zip(animal,how_many)
        #data = zip(form, reward_list)
        #print(data)
    return render(request, 'rewards/reward-custom.html', {'form': form, 'reward_list': reward_list})

def person_add_task(request):
    if request.method == "POST":
        form = RadioForm(request.POST)
        print(form)
        reward_list = Reward.objects.all()
        print("I am in IF!")
        if form.is_valid():
            print("I am in IF-IF!")
            print("Choice field is: ")
            print(form.cleaned_data['choice_field'])
            print("current user balance is original:")
            print(str(Person.objects.filter(user=User.objects.get(pk=1)).latest('action_date').current_balance))
            print("current user balance is request.user:")
            print(str(Person.objects.filter(user=request.user).latest('action_date').current_balance))
            print("task reward is request.user:")
            print("reward amount is : " + str(Reward.objects.get(id=form.cleaned_data['choice_field']).kikas))
            reward_kikas = Reward.objects.get(id=form.cleaned_data['choice_field']).kikas
            person_current_balance = Person.objects.filter(user=request.user).latest('action_date').current_balance
            final_balance = person_current_balance -  reward_kikas
            print("final_balance is : " + str(final_balance))
            Person.objects.create(user=request.user, action_date=timezone.now(), current_balance=final_balance)
            #reward = form.save(commit=False)
            #reward.author = request.user

            #reward.save()
            return redirect('person_add_task')
    else:
        form = RadioForm()
        print("I am in ELSE!")
        reward_list = Reward.objects.all()
        #print(form)
        #print(reward_list)
        #animal = ['cat','dog','mause']
        #how_many = ['one','two','three']
        #data = zip(animal,how_many)
        #data = zip(form, reward_list)
        #print(data)
    return render(request, 'rewards/person-add-task.html', {'form': form, 'reward_list': reward_list})


def reward_new(request):
    if request.method == "POST":
        form = RewardForm(request.POST)
        if form.is_valid():
            reward = form.save(commit=False)
            reward.author = request.user
            reward.save()
            return redirect('reward_detail', pk=reward.pk)
    else:
        form = RewardForm()
    return render(request, 'rewards/reward-edit.html', {'form': form})


def reward_edit(request, pk):
    reward = get_object_or_404(Reward, pk=pk)
    if request.method == "POST":
        form = RewardForm(request.POST, instance=reward)
        if form.is_valid():
            reward = form.save(commit=False)
            reward.author = request.user
            reward.save()
            return redirect('reward_detail', pk=reward.pk)
    else:
        form = RewardForm(instance=reward)
    return render(request, 'rewards/reward-edit.html', {'form': form})



class LineChartJSONView(BaseLineChartView):
    def get_labels(self):
        """Return 7 labels for the x-axis."""
        return ["0", "5", "10", "15", "20", "25", "30"]

    def get_providers(self):
        """Return names of datasets."""
        return ["A", "B", "C"]

    def get_data(self):
        """Return 3 datasets to plot."""

        return [[0, 13, 17, 30, 45, 50, 55],
                [0, 10, 15, 28, 30, 44, 47],
                [0, 5, 7, 10, 15, 23, 28]]

    def get_datasets(self):
        datasets = []
        #color_generator = self.get_colors()
        color_generator = [(255, 0, 0), (0, 255, 0), (255, 255, 0)]
        data = self.get_data()
        providers = self.get_providers()
        num = len(providers)
        for i, entry in enumerate(data):
            #color = tuple(next(color_generator))
            color = color_generator[i]
            print(color)
            dataset = {'backgroundColor': "rgba(%d, %d, %d, 0.5)" % color,
                       'borderColor': "rgba(%d, %d, %d, 1)" % color,
                       'pointBackgroundColor': "rgba(%d, %d, %d, 1)" % color,
                       'pointBorderColor': "#fff",
                        'fill' : False,
                       'data': entry}
            if i < num:
                dataset['label'] = providers[i]  # series labels for Chart.js
                dataset['name'] = providers[i]  # HighCharts may need this
            datasets.append(dataset)
        return datasets
    #    return ["rgba(255,0,0,0.3)", "rgba(0,255,0,1)", "rgba(255,255,0,1)"]



line_chart = TemplateView.as_view(template_name='line_chart.html')
line_chart_json = LineChartJSONView.as_view()

def ViewBaseChart(request):
    user_list = User.objects.all()
    context = {}
    template = "line_chart.html"

    return render(request,template,context, {'user_list': user_list})

class LineChartJSONView2(BaseLineChartView):
    def get_labels(self):
        """Return 7 labels for the x-axis."""
        return ["0", "5", "10", "15", "20", "25", "30"]

    def get_providers(self):
        """Return names of datasets."""
        return ["Good Balance", "Goal", "Bad Balance"]

    def get_data(self):
        """Return 3 datasets to plot."""

        return [[55, 43, 36, 20],
                [20, 20, 20, 20, 20, 20, 20],
                [20, 20, 20, 20, 15, 0]]

    def get_datasets(self):
        datasets = []
        #color_generator = self.get_colors()
        color_generator = [(0, 255, 0), (0, 0, 0), (255, 0, 0)]
        data = self.get_data()
        providers = self.get_providers()
        num = len(providers)
        for i, entry in enumerate(data):
            #color = tuple(next(color_generator))
            color = color_generator[i]
            print(color)
            dataset = {'backgroundColor': "rgba(%d, %d, %d, 0.5)" % color,
                       'borderColor': "rgba(%d, %d, %d, 1)" % color,
                       'pointBackgroundColor': "rgba(%d, %d, %d, 1)" % color,
                       'pointBorderColor': "#fff",
                        'fill' : False,
                       'data': entry}
            if i < num:
                dataset['label'] = providers[i]  # series labels for Chart.js
                dataset['name'] = providers[i]  # HighCharts may need this
            datasets.append(dataset)
        return datasets


line_chart2 = TemplateView.as_view(template_name='line_chart.html')
line_chart_json2 = LineChartJSONView2.as_view()

def ViewBaseChart2(request):

    context = {}
    template = "line_chart.html"

    return render(request,template,context)


from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _

from chartjs.colors import next_color, COLORS
from chartjs.views.columns import BaseColumnsHighChartsView
from chartjs.views.lines import BaseLineChartView, HighchartPlotLineChartView
from chartjs.views.pie import HighChartPieView, HighChartDonutView

class ColorsView(TemplateView):
    template_name = 'colors.html'

    def get_context_data(self, **kwargs):
        data = super(ColorsView, self).get_context_data(**kwargs)
        data['colors'] = islice(next_color(), 0, 50)
        return data
