from django.contrib import admin
from django.urls import path
from . import views
from django.conf import settings
from django.views.generic import TemplateView
from .views import LineChartJSONView, LineChartJSONView2

from chartjs.colors import next_color, COLORS
from chartjs.views.columns import BaseColumnsHighChartsView
from chartjs.views.lines import BaseLineChartView, HighchartPlotLineChartView
from chartjs.views.pie import HighChartPieView, HighChartDonutView

urlpatterns = [
    path('', views.reward_list, name='reward_list'),
    #path('', views.person_personlist, name='person_list'),
    path('<int:pk>/', views.reward_detail, name='reward_detail'),
    path('new/', views.reward_new, name='reward_new'),
    path('<int:pk>/edit/', views.reward_edit, name='reward_edit'),
    path('goods/', views.reward_goods, name='reward_goods'),
    path('custom/', views.reward_custom, name='reward_custom'),
    path('screentime/', views.reward_screentime, name='reward_screentime'),
    path('tasklist/', views.task_tasklist, name='task_tasklist'),
    path('personlist/', views.person_personlist, name='person_list'),
    path('person-add-task/', views.person_add_task, name='person_add_task'),
    path('charts_data/', LineChartJSONView.as_view(), name='line_chart_json'),
    path('charts_data2/', LineChartJSONView2.as_view(), name='line_chart_json2'),
    path('charts/', views.ViewBaseChart, name='basic_chart'),
    path('charts2/', views.ViewBaseChart2, name='basic_chart2'),
    path('colors/', views.ColorsView, name='colors'),
    path('investments/', views.reward_investments, name='reward_investments'),
    path('blank/', views.reward_blank, name='reward_blank'),


    #path('add', views.RewardCreateView.as_view(), name='add'),
    #path('<slug:reward_slug>', views.reward_detail, name='detail'),
]

"""
if settings.DEBUG:
    urlpatterns += path('',
         ('media/(?P.*)$',
        'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}))
"""
