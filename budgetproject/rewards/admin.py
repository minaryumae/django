from django.contrib import admin
from .models import Reward, Task, Person

admin.site.register(Reward)
admin.site.register(Task)
admin.site.register(Person)

# Register your models here.
