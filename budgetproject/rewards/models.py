from django.db import models
from django.conf import settings
from django.utils import timezone


# Create your models here.
class Reward(models.Model):
    name = models.CharField(max_length=100)
    picture = models.ImageField(upload_to='images/', null=True)
    kikas = models.IntegerField()
    category = models.CharField(max_length=100, default='Goods')

    def __str__(self):
        return self.name

class Task(models.Model):
    name = models.CharField(max_length=100)
    picture = models.ImageField(upload_to='images/', null=True)
    kikas = models.IntegerField()

    def __str__(self):
        return self.name

"""
from django.contrib.auth.models import User

def get_first_name(self):
    return self.first_name

User.add_to_class("__str__", get_first_name)
"""

class Person(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    action_date = models.DateTimeField(default=timezone.now)
    current_balance = models.IntegerField()
    #reward_amount = models.ForeignKey(Reward, on_delete=models.CASCADE)
    #task_amount = models.ForeignKey(Task, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.get_username()


"""
class Project(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True, blank=True)
    budget = models.IntegerField()

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Project, self).save(*args, **kwargs)

    def budget_left(self):
        expense_list = Expense.objects.filter(project=self)
        total_expense_amount = 0
        for expense in expense_list:
            total_expense_amount += expense.amount

        return self.budget - total_expense_amount

    def total_transactions(self):
        expense_list = Expense.objects.filter(project=self)
        return len(expense_list)
"""