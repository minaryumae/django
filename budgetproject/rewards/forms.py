from django import forms

from .models import Reward, Person 

class RewardForm(forms.ModelForm):

    class Meta:
        model = Reward 
        fields = ('name', 'kikas',)

class PersonForm(forms.ModelForm):

    class Meta:
        model = Person 
        fields = ('user', 'action_date', 'current_balance',)

class RadioForm(forms.Form):
    CHOICES = (('1', 'First'), ('2', 'Second'), ('3', 'Third'), ('4', 'Fourth'), ('5', 'Fifth'))
    choice_field = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES)



class ScheduleForm(forms.Form):
    NOW, LATER = 'now', 'later'
    SCHEDULE_CHOICES = (
        (NOW, 'Send immediately'),
        (LATER, 'Send later'),
    )
    schedule = forms.ChoiceField(
        choices=SCHEDULE_CHOICES, widget=forms.RadioSelect)
    send_date = forms.DateTimeField(
        label="", required=False)

    """
    def __init__(self, data=None, *args, **kwargs):
        super(ScheduleForm, self).__init__(data, *args, **kwargs)

        # If 'later' is chosen, set send_date as required
        if data and data.get('schedule', None) == self.LATER:
            self.fields['send_date'].required = True
    """